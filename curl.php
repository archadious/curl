<?php
// Time-stamp: <2020-04-09 20:16:22 daniel>
// curl.php

// Copyright (c) 2020 Daniel Mendyke  All Rights Reserved.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Wrapper around Curl requests using PHP
//
// Example use:
//
//  $curl = new Curl();
//  $html = $curl->get( "http://localhost" );
//

// Wrapper around libcurl calls
//-----------------------------------------------------------------------------
class Curl {


  // Instance varables
  //---------------------------------------------------------------------------
  private $handle = FALSE;  // will hold curl object


  // Initialize an instance of this class
  //---------------------------------------------------------------------------
  function __construct( ) {
    $this->handle = $this->exception( curl_init( ) );
  }  // end constructor


  // Clean up after use of an instance of this class
  //---------------------------------------------------------------------------
  function __destruct( ) {
    if ( $this->handle != FALSE ) {
      curl_close( $this->handle );
    };  // end if handle is not false
  }  // end destructor


  //---------------------------------------------------------------------------
  function exception( $value ) {
    if ( $value === FALSE ) {
      throw new Exception( curl_error( $this->handle ) );
    };  // end if value is false
    return $value;
  }  // end exception


  //---------------------------------------------------------------------------
  function option( $opt, $value ) {
    $this->exception( curl_setopt( $this->handle, $opt, $value ) );
  }  // end option


  // Set standand options then call actual curl execution
  //---------------------------------------------------------------------------
  function exec( $url ) {
    $this->option( CURLOPT_URL, $url );
    $this->option( CURLOPT_RETURNTRANSFER, TRUE );
    return $this->exception( curl_exec( $this->handle ) );
  }  // end exec


  // Perform a get request
  //---------------------------------------------------------------------------
  function get( $url ) {
    return $this->exec( $url );
  }  // end get


  // Perform a post request
  //---------------------------------------------------------------------------
  function post( $url, $payload ) {
    $this->option( CURLOPT_POSTFIELDS, $payload );
    return $this->exec( $url );
  }  // end post


}  // end class Curl
